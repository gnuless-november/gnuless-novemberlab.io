# GNULess November: This November, purge GNU for a month.

GNULess November is a fairly simple challenge to understand:
1. On November 1st, uninstall any GNU software (e.g. gcc, coreutils)
from your system. You may want to install a special GNUless distro
such as Alpine.
2. Use your newly GNUless system as your daily driver for a whole month.
3. On November 31st, you can reinstall your GNU tools/switch back to your
GNU/Linux distro (or don't! If you find that you prefer to not use GNU,
then keep it that way.)

## The Purpose of the Challenge

The most accurate way to honestly describe the purpose of this challenge
is that my friends and I thought it sounded cool, so we put it up here.

That being said, doing this challenge could have actual interesting
results. We wanted to see just how much GNU software actually matters
in day-to-day usage (especially considering that a lot of people call Linux GNU/Linux)
and whether non-GNU alternatives actually worked better. So by undertaking
this challenge, you can both increase your knowledge of how your distro/OS
works (by identifying GNU components) and experiment to see if a
non-GNU workflow fits better!

Note that this challenge is not meant to antagonize GNU and the FSF in any way.
It's purely a challenge to see how much GNU software you use on a daily
basis and how it would feel to get rid of/replace these components.

## Rules of the Challenge

As stated before, the challenge is pretty simple. There's only a few rules:
1. From November 1 to November 31, you must have **no GNU software**
on ur system. This includes glibc, the gcc compiler, any system daemons, etc.
To assist you in this, you can try a non-GNU based distro such as Alpine.
2. A special exception to this - software *precompiled* with gcc is OK. For
example, if you use a binary distro and the kernel is precompiled with GCC,
it's fine to use it.
3. It's up to you whether GNU libraries used in other programs count (e.g. GTK,
which is part of GNOME, which is part of GNU). Since a huge amount of Linux programs
depend on a GNU library in some way, we feel that banning all GNU libraries would likely
be too difficult and get in the way of the actual usefulness of the challenge - but
if you really want a challenge, feel free to do it! (It's up to you to decide which
GNU libraries are ok and which are too much).
4. This challenge only counts for software *currently under* the GNU project,
not software that was originally under the GNU project that was since transferred
to other maintainers.

## Tips & Notes

- You probably want to install a non-GNU based Linux distro, such as Alpine,
alongside your existing distro. That way, if you decide by November 31 that
the GNUless life just isn't for you, you can go back to your old distro with
no interruptions.
- Many seemingly essential GNU tools have replacements! For example, busybox/toybox
can replicate most of the functionality of GNU coreutils, and glibc can be replaced
with libmuslc or another alternative standard library. Most non-GNU distros such as
Alpine will replace most of this software for you.
- Watch out for dependencies and other tricks that might accidentally pull in GNU software!
Again, a special non-GNU distro will solve this problem.

## Contributing

Want to leave some tips for your fellow GNUless November participants, or
add or modify anything else? Submit a PR!

If you want to get in touch directly, let me know via vwangsf@gmail.com. Thanks!
